extern "C" {

	#include <efi.h>
	#include <efilib.h>

}

#include "std.hpp"
#include "sheap.hpp"
#include "io.hpp"
#include "calc.hpp"

void cxx_main();

extern "C" {

	EFI_STATUS
	EFIAPI
	efi_main (EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable) {
	   InitializeLib(ImageHandle, SystemTable);
	   SystemTable->BootServices->SetWatchdogTimer(0, 0, 0, NULL);
	   mem::alloc.Init(mem::HeapSize);
	   cxx_main();
	   return EFI_SUCCESS;
	}

}

void cxx_main() {
	io::Input in;
	for(;;) {
		puts(L"Input: \n");
		efi::string line;
		in.read_line(line);
		printf(L"\nOutput: %lf\n", calc::compute(line));
		io::print_string(line);
	}

	panic();
	
}
