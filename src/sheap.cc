#include "sheap.hpp"
#include <cstring>
#include "std.hpp"
namespace mem{

Allocator::Allocator(){

}

void Allocator::Init(std::size_t totalSize){
	heap.totalSize = totalSize;
	heap.currentSize = 0;
	auto pages = allocPages(totalSize >> 12);
	heap.head = reinterpret_cast<__mem_chunk*>(pages);
	if(heap.head == 0){
		puts(L"Cannot allocate heap.\n");
		panic();
	}
	heap.tail = heap.head;
	heap.head->size = totalSize - sizeof(__mem_chunk);
	heap.head->next = 0;
	heap.head->prev = 0;
	heap.head->used = false;
}

void * Allocator::Malloc(std::size_t size){
	__mem_chunk * cur = heap.head;
	if(size % 8 != 0){
		size += 8 - size % 8;
	}
	while(cur){
		if(cur->used || cur->size <= size + sizeof(__mem_chunk)){
			cur = cur->next;
			continue;
		}
		__mem_chunk * next = reinterpret_cast<__mem_chunk*>( reinterpret_cast<char*>(cur) + sizeof(__mem_chunk) + size);
		next->size = cur->size - sizeof(__mem_chunk) - size;
		next->used = false;
		if(cur->next){
			cur->next->prev = next;
			next->next = cur->next;
		}else{
			next->next = 0;
		}
		next->prev = cur;
		cur->next = next;
		cur->used = true;
		cur->size = size;
		return reinterpret_cast<void*>(reinterpret_cast<u64>(cur) + sizeof(__mem_chunk));
	}
	return 0;
}

void Allocator::Free(void * ptr){
	__mem_chunk * cur = reinterpret_cast<__mem_chunk*>(reinterpret_cast<u64>(ptr)-sizeof(__mem_chunk));
	cur->used = 0;
	Consolidate();
}

void Allocator::Consolidate(){
	__mem_chunk * cur = heap.head;
	__mem_chunk * begin = 0;
	while(cur){
		if(!cur->used){
			begin = cur;
			for(; cur->next && !cur->next->used; cur=cur->next);
			if(cur != begin){
				begin->next = cur->next;
				if(cur->next){
					cur->next->prev = begin;
				}
				begin->size = reinterpret_cast<u64>(cur) - reinterpret_cast<u64>(begin) + cur->size;
			}
		}
		cur=cur->next;
	}
}

void * Allocator::Calloc(std::size_t size,std::size_t sizeOfItem){
	auto ptr = Malloc(size * sizeOfItem);
	memset(ptr,0,size*sizeOfItem);
	return ptr;
}
void * Allocator::Realloc(void * ptr,std::size_t newSize){
	auto nptr = Malloc(newSize);
	__mem_chunk * cur = reinterpret_cast<__mem_chunk*>(reinterpret_cast<u64>(ptr) - sizeof(__mem_chunk));
	memcpy(nptr,ptr,cur->size);
	Free(ptr);
	Consolidate();
}

Allocator alloc;

void * allocPages(std::size_t n){
	void * ret = reinterpret_cast<void*>(0x100000000);
	EFI_STATUS efiStatus = uefi_call_wrapper((void*)BS->AllocatePages,4,
		AllocateMaxAddress,
		EfiRuntimeServicesData,
		n,
		reinterpret_cast<EFI_PHYSICAL_ADDRESS*>(&ret));
	if(efiStatus != EFI_SUCCESS){
		puts(L"Fuck up at alloc.\n");
		return 0;
	}
	//AllocatedPages->push_back(ret);
	return ret;
}

}