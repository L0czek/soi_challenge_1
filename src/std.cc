#include "std.hpp"

Protocol::Protocol(const EFI_GUID & request):guid(request){
	EFI_GUID copy = request;
	UINTN size = 0;
	EFI_STATUS efistatus = uefi_call_wrapper((void*)BS->LocateHandle,5,ByProtocol,&copy,NULL,&size,handles.data());
	handles.resize(size/sizeof(EFI_HANDLE));
	efistatus = uefi_call_wrapper((void*)BS->LocateHandle,5,ByProtocol,&copy,NULL,&size,handles.data());
	if(efistatus != EFI_SUCCESS){
		handles.clear();
	}
}

EFI_STATUS WaitFor(EFI_EVENT event){
	UINTN tmp;
	return uefi_call_wrapper((void*)BS->WaitForEvent,3,1,&event,&tmp);
}
UINTN WaitFor(efi::vector<EFI_EVENT> & events){
	UINTN ret = 0;
	if(uefi_call_wrapper((void*)BS->WaitForEvent,3,events.size(),events.data(),&ret) != EFI_SUCCESS){
		return -1;
	}
	return ret;
}