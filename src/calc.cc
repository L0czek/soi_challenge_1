#include "calc.hpp"

#include <string>
#include <cmath>
#include "math.hpp"
#include <cstring>

namespace calc {

struct Token {
	enum class Type {
		Invalid,
		Number,
		Operator
	}type = Type::Invalid;
	double number = 0.0;
	char op = 0;
};

const char* strchr(const char* str, char c) {
	while(*str && *str != c) {
		str++;
	}
	return *str == c ? str : nullptr;
}

double stod(efi::string::const_iterator first, efi::string::const_iterator last) {
	double ret = 0.0;
	for(; first <= last; ++first) {
		ret += (*first - '0');
		ret *= 10.0;
	}
	return ret / 10.0;
}

efi::vector<Token> tokenize(const efi::string& line) {
	efi::vector<Token> tokens;
	auto first = line.cbegin();
	auto end = line.cend() - 1;
	for(; first < end; ++first) {
		const auto ch = *first;
		if(strchr(" \t", ch) != nullptr) {
			continue;
		}
		if(ch >= '0' && ch <= '9') {
			const auto num_beg = first;
			while(*first >= '0' && *first <= '9' && first < end)
				first++;
			const auto num_end = first - 1;
			tokens.push_back({
				Token::Type::Number,
				stod(num_beg, num_end),
				0
			});
			first--;
		} else if(strchr("-+*/()^", ch) != nullptr) {
			if(ch == '-' && (first == line.cbegin() || strchr("(-+*/^ \t", *(first-1)) != nullptr)) {
				tokens.push_back({
					Token::Type::Number,
					0.0,
					0
				});
			}
			tokens.push_back({
				Token::Type::Operator,
				0.0,
				ch
			});
		} else {
			printf(L"invalid char %c\n", ch);
			tokens.push_back({
				Token::Type::Invalid,
				0.0,
				0
			});
		}
	}
	return tokens;
}

std::size_t get_presidence(char op) {
	switch(op) {
		case '+':
		case '-':
			return 1;
			break;
		case '*':
		case '/':
			return 2;
			break;
		case '(':
		case ')':
			return 3;
			break;
		case '^':
			return 4;
			break;
		default:
			return 0;
	}
}

efi::vector<Token> shunting_yard(const efi::vector<Token>& tokens, bool & ok) {
	efi::vector<Token> ret;
	efi::stack<char> op_stack;
	for(const auto &i : tokens) {
		switch(i.type) {
			case Token::Type::Number:
				ret.push_back(i);
				break;
			case Token::Type::Invalid:
				ok = false;
				return ret;
			case Token::Type::Operator:
				switch(i.op) {
					case '+':
					case '-':
					case '*':
					case '/':
					case '^':
						while((!op_stack.empty() && get_presidence(op_stack.top()) >= get_presidence(i.op)) &&
								(!op_stack.empty() && op_stack.top() != '(')) {
									auto op = op_stack.top();
									op_stack.pop();
									ret.push_back({
										Token::Type::Operator,
										0.0,
										op
									});
							}
							op_stack.push(i.op);
						break;
					case '(':
						op_stack.push('(');
						break;
					case ')':
						while((!op_stack.empty() && op_stack.top() != '(')){
							auto ch = op_stack.top();
							op_stack.pop();
							ret.push_back({
								Token::Type::Operator,
								0.0,
								ch
							});
						}
						if(!op_stack.empty() && op_stack.top() == '('){
							op_stack.pop();
						}
						break;
				}
				break;
		}
	}
	while(!op_stack.empty()) {
		if(op_stack.top() != '(') {
			ret.push_back({
				Token::Type::Operator,
				0.0,
				op_stack.top()
			});
		}
		op_stack.pop();
	}
	return ret;
}

double calculate(const efi::vector<Token>& tokens) {
	efi::stack<double> stack;
	for(const auto &i : tokens) {
		switch(i.type) {
			case Token::Type::Number:
				stack.push(i.number);
				break;
			case Token::Type::Operator:
				if(stack.size() < 2) {
					return 0;
				}
				const auto b = stack.top(); stack.pop();
				const auto a = stack.top(); stack.pop();
				switch(i.op) {
					case '+': stack.push(a + b); break;
					case '-': stack.push(a - b); break;
					case '*': stack.push(a * b); break;
					case '/': stack.push(a / b); break;
					case '^': stack.push(pow(a,b)); break;
				}
		}
	}
	if(stack.size() != 1) {
		return 0;
	} else {
		return stack.top();
	}
}

double compute(const efi::string& line) {
	auto tokens = tokenize(line);
	//for(const auto &i : tokens) {
	//	switch(i.type) {
	//		case Token::Type::Number: printf(L"%lf, ", i.number); break;
	//		case Token::Type::Operator: printf(L"%c, ", i.op); break;
	//	}	
	//}
	//puts(L"\n");
	bool ok = true;
	const auto rtokens = shunting_yard(tokens, ok);
	if(ok == true) {
		return calculate(rtokens);
	}
}

}