#include "io.hpp"



namespace io{

EFI_FILE_PROTOCOL * OpenFile(wc * name){
	Protocol p(EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID);
	EFI_GUID sfspGuid = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
	EFI_STATUS efiStatus;
	for(auto i=0; i < p.getHandles().size(); i++){
		EFI_SIMPLE_FILE_SYSTEM_PROTOCOL* fs = p.Handle<EFI_SIMPLE_FILE_SYSTEM_PROTOCOL>(i);
      	if(fs == 0){
      		return 0;
      	}
     	EFI_FILE_PROTOCOL* root = 0;
    	efiStatus = uefi_call_wrapper((void*)fs->OpenVolume,2,fs, &root);
    	if(efiStatus != EFI_SUCCESS){
      		return 0;
      	}
    	EFI_FILE_PROTOCOL* token = 0;
    	efiStatus = uefi_call_wrapper((void*)root->Open,5,
        	root, 
         	&token,
         	(CHAR16*)name,
         	EFI_FILE_MODE_READ,
         	EFI_FILE_READ_ONLY | EFI_FILE_HIDDEN | EFI_FILE_SYSTEM);
   		if(efiStatus == EFI_SUCCESS){
       		return token;
    	}
	}
	return 0;
}
char * ReadFile(EFI_FILE_PROTOCOL * file){
	if(file == 0){
		return 0;
	}
	EFI_FILE_INFO * info;
	EFI_GUID finfoid = gEfiFileInfoGuid;
	UINTN size = 0;

	EFI_STATUS efiStatus = uefi_call_wrapper((void*)file->GetInfo,4,file,&finfoid,&size,info);
	info = reinterpret_cast<EFI_FILE_INFO*>(mem::alloc.Malloc(size));
	if(info == 0){
		return 0;
	}
	efiStatus = uefi_call_wrapper((void*)file->GetInfo,4,file,&finfoid,&size,info);
	if(efiStatus != EFI_SUCCESS){
		free(info);
		return 0;
	}
	UINTN fsize = info->FileSize;
	char * ret = reinterpret_cast<char*>(mem::alloc.Malloc(fsize));
	free(info);
	if(ret == 0){
		return 0;
	}
	efiStatus = uefi_call_wrapper((void*)file->Read,3,file,&fsize,ret);
	if(efiStatus != EFI_SUCCESS){
		free(ret);
		return 0;
	}
	return ret;
}
void CloseFile(EFI_FILE_PROTOCOL * file){
	if(file == 0){
		return;
	}
	uefi_call_wrapper((void*)file->Close,1,file);
}

Input::Input(){
	Protocol p(EFI_SIMPLE_TEXT_INPUT_PROTOCOL_GUID);
	total_inputs = p.size();
	for(std::size_t i=0; i < p.size(); ++i) {
		const auto ptr = p.Handle<EFI_SIMPLE_TEXT_IN_PROTOCOL>(i);
		if(ptr == nullptr) {
			puts(L"Invalid current_input() protocol.");
			panic();
		}
		inputs.push_back(ptr);
	}
}

char Input::getchar(){
	EFI_INPUT_KEY key;
	if(uefi_call_wrapper((void*)current_input()->ReadKeyStroke,2,current_input(),&key) != EFI_SUCCESS){
		return 0;
	}
	return static_cast<char>(key.UnicodeChar);
}

wchar_t Input::getwchar(){
	EFI_INPUT_KEY key;
	if(uefi_call_wrapper((void*)current_input()->ReadKeyStroke,2,current_input(),&key) != EFI_SUCCESS){
		return 0;
	}
	return key.UnicodeChar;
}

bool Input::getLine(efi::string & out,char d){
	if(current_input() == NULL){
		return false;
	}
	char c = getchar();
	if(c==0){
		return false;
	}
	out.clear();
	do{
		out += c;
		WaitFor(current_input()->WaitForKey);
		c = getchar();
	}while(c && c != d);
	return true;
}

bool Input::getLine(efi::wstring & out,wchar_t d){
	if(current_input() == NULL){
		return false;
	}
	wchar_t c = getwchar();
	if(c==0){
		return false;
	}
	out.clear();
	do{
		out += c;
		WaitFor(current_input()->WaitForKey);
		c = getwchar();
	}while(c&& c != d);
	return true;
}

u16 Input::getScanCode(){
	EFI_INPUT_KEY key;
	if(uefi_call_wrapper((void*)current_input()->ReadKeyStroke,2,current_input(),&key) != EFI_SUCCESS){
		return 0;
	}
	return key.ScanCode;
}

EFI_EVENT Input::getEvent(){
	return current_input() ? current_input()->WaitForKey : 0;
}

}