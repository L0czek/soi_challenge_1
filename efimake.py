#!/usr/bin/python

import os
import sys
import json
import subprocess



CC = 'g++'
LD = 'ld'
OB = 'objcopy'
AS = "nasm"

EFIINC = 'inc/gnu-efi'
EFILIB = 'lib'

ASMFLAGS = '-f elf64'.split()

CFLAGS = '-c -masm=intel -fno-stack-protector -fpic -D_GLIBCXX_FULLY_DYNAMIC_STRING -fshort-wchar -Wall -Wextra -mno-red-zone -Iinc -I{0}  -I{0}/x86_64 -DEFI_FUNCTION_WRAPPER -std=c++17 -ggdb -O0'.format(EFIINC).split()

LDFLAGS = '{0}/crt0-efi-x86_64.o -static  -nostdlib -znocombreloc -Tscripts/elf_x86_64_efi.lds -shared -Bsymbolic -L{0} -l:libgnuefi.a -l:libefi.a'.format(EFILIB).split()
OBJCOPYFLAGS = '-j .text -j .sdata -j .data -j .dynamic -j .dynsym -j .rel -j .rela -j .reloc -j .init_array -j .fini_array --rename-section .init_array=.init --rename-section .fini_array=.fini --target=efi-app-x86_64'.split()

UEFI = 'bios/OVMF.fd'

cmds = {
	'CR_FINAL' : 'dd if=/dev/zero of={} bs=512 count=93750',
	'OVWR' : 'dd if={}/tmp.img of={} bs=512 count=91669 seek=2048 conv=notrunc',
	'CR_TMP' : 'dd if=/dev/zero of={}/tmp.img bs=512 count=91669',
	'PARTED_CMD' : [
		'sudo parted {} -s -a minimal mklabel gpt',
		'sudo parted {} -s -a minimal mkpart EFI FAT16 2048s 93716s',
		'sudo parted {} -s -a minimal toggle 1 boot'
	],
	'MCOPY' : 'mcopy -i {}/tmp.img {} ::',
	'MFORMAT' : 'mformat -i {}/tmp.img -h 32 -t 32 -n 64 -c 1',
	'QEMU' : 'qemu-system-x86_64 -cpu host,vmx=on -bios {} -drive file={},if=ide -s -serial stdio -enable-kvm'
}

CONFIG_FILE = 'config.json'
CONFIG = {}
OUT_FILES = ['bin/BOOTX64.efi']
targets = []
FINAL_IMG = 'boot.img'
IMG_DIR = 'img'

def mkdir(path):
	if not os.path.isdir(path):
		os.mkdir(path)

def readConfig():
	global CONFIG,targets,FINAL_IMG,IMG_DIR
	try:
		with open(CONFIG_FILE,"rb") as f:
			CONFIG = json.loads(f.read())
			if 'targets' in CONFIG:
				targets = CONFIG['targets']
			else:
				targets = []
			if 'FINAL_IMG' in CONFIG:
				FINAL_IMG = CONFIG["FINAL_IMG"]
			if 'IMG_DIR' in CONFIG:
				IMG_DIR = CONFIG["IMG_DIR"]
	except:
		CONFIG = {}
		with open(CONFIG_FILE,"wb") as f:
			f.write('')

def checkFTime(target):
	ret = False
	for i in os.listdir(target["src"]):
		if not os.path.isfile(target["src"]+"/"+i):
			continue
		if  i not in target["ftime"] or os.path.getmtime(target["src"]+"/"+i) != target["ftime"][i]:
			ret = True
			target["ftime"][i] = os.path.getmtime(target["src"]+"/"+i)
	return ret

def makeTarget(name,src,bin,obj):
	targets.append({'name':name,'src':src,'bin':bin,'obj':obj,'ftime':{}})

def run_cmd(cmd):
	if type(cmd) is str:
		cmd = cmd.split()
	sys.stdout.write("\nExecuting: {}\n".format(' '.join(cmd)))
	try:
		subprocess.check_call(cmd)
	except:
		print "\n\n\nError while processing {}\n\n\n".format(' '.join(cmd))
		exit()

def createTMPimg():
	run_cmd(cmds["CR_TMP"].format(IMG_DIR))
	run_cmd(cmds["MFORMAT"].format(IMG_DIR))

def createFinalimg():
	run_cmd(cmds["CR_FINAL"].format(IMG_DIR + "/" + FINAL_IMG))
	for i in cmds["PARTED_CMD"]:
		run_cmd(i.format(IMG_DIR + "/" + FINAL_IMG))

def mcopy():
	for i in OUT_FILES:
		run_cmd(cmds["MCOPY"].format(IMG_DIR,i))
def ovr():
	run_cmd(cmds["OVWR"].format(IMG_DIR,IMG_DIR+"/"+FINAL_IMG))

def run_QEMU():
	run_cmd(cmds["QEMU"].format(UEFI,IMG_DIR+"/"+FINAL_IMG))

def writeConfig():
	CONFIG['targets'] = targets
	CONFIG['IMG_DIR'] = IMG_DIR
	CONFIG['FINAL_IMG'] = FINAL_IMG
	with open(CONFIG_FILE,"wb") as f:
		f.write(json.dumps(CONFIG))

def compile(target):
	if not checkFTime(target):
		sys.stdout.write("Target {} not modyfied exiting.\n".format(target['name']))
		return 0;
	files = [i for i in os.listdir(target["src"]) if os.path.isfile(target["src"] + "/" + i) ]
	cc_files = [ i for i in files if i.split('.')[1] == "cc" or i.split('.')[1] == "c" ]
	asm_files = [ i for i in files if i.split('.')[1] == "asm" ]
	bname = target["name"].split('.')[0]
	obj_files = []
	for i in cc_files:
		cc_cmd = [CC,target["src"]+"/"+i] + CFLAGS + ["-o",target["obj"]+"/"+i.split('.')[0]+".o"]
		run_cmd(cc_cmd)
		obj_files.append(target["obj"]+"/"+i.split('.')[0]+".o")
	for i in asm_files:
		asm_cmd = [AS,target["src"]+"/"+i] + ASMFLAGS + ["-o",target["obj"]+"/"+i.split('.')[0]+".o"]
		run_cmd(asm_cmd)
		obj_files.append(target["obj"]+"/"+i.split('.')[0]+".o")
	ld_cmd = [LD] + obj_files + LDFLAGS + ["-o",target["bin"]+"/"+bname+".so"]
	run_cmd(ld_cmd)
	obj_cmd = [OB] + OBJCOPYFLAGS + [target["bin"]+"/"+bname +".so",target["bin"]+"/"+bname +".efi"]
	run_cmd(obj_cmd)
	OUT_FILES.append(target["bin"]+"/"+bname +".efi")
	return 1

def buildImg():
	createFinalimg()
	createTMPimg()
	mcopy()
	ovr()

def make():
	status = 0
	for i in targets:
		status += compile(i)
	if status == 0:
		sys.stdout.write("Project not modyfied no img has to be created.\n")
	else:
		buildImg()

def main(argc,argv):
	readConfig()
	if argc == 2:
		if argv[1] == '--make':
			make()
			writeConfig()
			run_QEMU()
		elif argv[1] == '--run':
			run_QEMU()
		elif argv[1] == '--rebuild':
			for i in targets:
				i["ftime"] = {}
			make()
			run_QEMU()
		elif argv[1] == '--rm-config':
			os.remove(CONFIG_FILE)
			sys.exit()
		elif argv[1] == '--compile':
			make()

	elif argc == 4:
		if argv[1] == "--setup":
			global IMG_DIR,FINAL_IMG
			IMG_DIR = argv[3]
			FINAL_IMG = argv[2]
			mkdir(IMG_DIR)
	elif argc == 6:
		if argv[1] == "--add-target":
			makeTarget(*argv[2:6])
			for i in argv[3:6]:
				mkdir(i)
	writeConfig()

if __name__ == "__main__":
	main(len(sys.argv),sys.argv)
