#pragma once

#include "std.hpp"
#include "sheap.hpp"

#include <iterator>
#include <type_traits>

namespace io{

using namespace mem;

EFI_FILE_PROTOCOL * OpenFile(wc * name);

char * ReadFile(EFI_FILE_PROTOCOL * file);
void CloseFile(EFI_FILE_PROTOCOL * file);

template<typename C>
void print_string(const C& c, const wchar_t* end = L"\n") {
	for(const auto &i : c) {
		printf(L"%c", i);
	}
	puts(end);
}

class Input{
	efi::vector<EFI_SIMPLE_TEXT_IN_PROTOCOL*> inputs;
	std::size_t current_source = 0;
	std::size_t total_inputs;
	auto current_input() {
		return inputs[current_source];
	}
	void next_input() {
		current_source++;
		current_source %= total_inputs;
		printf(L"switching to %d input\n", current_source);
	}
public:
	Input();
	char getchar();
	wchar_t getwchar();
	bool getLine(efi::string & out,char d = '\r');
	bool getLine(efi::wstring & out,wchar_t d = '\r');
	u16 getScanCode();
	EFI_EVENT getEvent(); 
	template<typename C>
	void read_line(C & c) {
		typename C::value_type ch = 0;
		auto it = std::back_inserter(c);
		const auto& event = getEvent();
		do {
			ch = read_char<typename C::value_type>();
			if(ch == static_cast<typename C::value_type>(8)) { // Backspace
				if(c.size() > 0){
					puts(L"\r");
					for(std::size_t i=0; i < c.size(); ++i){
						puts(L" ");
					}
					puts(L"\r");
					c.pop_back();
					it = std::back_inserter(c);
				}
			} else {
				it = ch;
			}
			print_string(c, L"\r");
		}while(ch != static_cast<typename C::value_type>(13));
	}
	template<typename T>
	T read_char() {
		for(;;) {
			const auto event = getEvent();
			if(WaitFor(event) != EFI_SUCCESS){
				next_input();
				continue;
			} else {
				if constexpr(std::is_same<T, char>::value) {
					return getchar();
				} else {
					return getwchar();
				}
			}
		}
	}
};


}