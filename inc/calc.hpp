#pragma once

#include "std.hpp"

namespace calc {
	double compute(const efi::string& line);
}