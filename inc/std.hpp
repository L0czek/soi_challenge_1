#pragma once

extern "C" {

	#include <efi.h>
	#include <efilib.h>

}

#include <vector>
#include <string>
#include <deque>
#include <stack>
#include "sheap.hpp"

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;
using wc = CHAR16;

#define printf(fmt,...) Print((CHAR16*)fmt,__VA_ARGS__)
#define puts(text) Print((CHAR16*)text)
#define inv(func,...) uefi_call_wrapper((void*)func,__VA_ARGS__)
#define call(func) uefi_call_wrapper((void*)func)


inline void panic() { puts(L"System Halted.\n\n"); for(;;); }

namespace efi {

	template<typename T> using vector = std::vector<T,mem::allocator<T>>;
	template<class CharT, 
    	 class Traits = std::char_traits<CharT>, 
    	 class Allocator = mem::allocator<CharT>
    	 > using basic_string = std::basic_string<CharT,Traits,Allocator>;

   	template<typename T> using deque = std::deque<T, mem::allocator<T>>;
    template<typename T> using stack = std::stack<T, deque<T>>;

	typedef basic_string<char> string;
	typedef basic_string<wchar_t> wstring;
	typedef basic_string<CHAR16> chstring;

}


EFI_STATUS WaitFor(EFI_EVENT event);
UINTN WaitFor(efi::vector<EFI_EVENT> & events);

class Protocol{
	EFI_GUID guid;
	efi::vector<EFI_HANDLE> handles;
public:
	Protocol(const EFI_GUID & request);
	efi::vector<EFI_HANDLE> & getHandles();
	template<typename T>
	T * Handle(std::size_t n = 0){
		T * ret;
		if(n >= handles.size()){
			return 0;
		}
		if(
			uefi_call_wrapper((void*)BS->HandleProtocol,3,handles[n],&guid,&ret) != EFI_SUCCESS
		){
			return 0;
		}else{
			return ret;
		}
	}
	std::size_t size() const {
		return handles.size();
	}
};

