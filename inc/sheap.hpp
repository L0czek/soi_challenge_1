#pragma once

#include <cstdlib>
#include <cstdint>
namespace mem{

static const std::size_t HeapSize = 1024*1024*4;

struct __mem_chunk{
	__mem_chunk * next;
	__mem_chunk * prev;
	std::size_t size;
	bool used;
};

struct __heap{
	__mem_chunk * head;
	__mem_chunk * tail;
	std::size_t currentSize;
	std::size_t totalSize;
};

class Allocator{
	__heap heap;
public:
	Allocator();
	void * Malloc(std::size_t size);
	void * Calloc(std::size_t size,std::size_t sizeOfItem);
	void * Realloc(void * ptr,std::size_t newSize);
	void Free(void * ptr);
	void Consolidate();
	void Init(std::size_t size);
};

extern Allocator alloc;

template<typename T>
struct allocator{
	template<typename K>
	allocator(K k) {

	}
	allocator() {
		
	}
	typedef T value_type;
	T * allocate(std::size_t n){
		return reinterpret_cast<T*>(alloc.Malloc(n*sizeof(T)));
	}
	void deallocate(T * t,std::size_t){
		alloc.Free(t);
	}
};

void * allocPages(std::size_t n);

}