#pragma once

__attribute__((naked)) inline double sqrt(double number) {
	__asm(
	        "sub rsp, 16 \n"
	        "movsd qword ptr [rsp], xmm0\n"
            "fld qword ptr [rsp]\n"
            "fsqrt\n"
            "fstp qword ptr [rsp]\n"
            "movsd xmm0, qword ptr [rsp]\n"
            "add rsp, 16 \n"
	        "ret \n"
	        
	    );
}

double sqr(double d) {
	return d*d;
}

double pow(double base, double power, double precision) {    
	if (power < 0) 
		return 1 / pow( base, -power, precision );
	if (power >= 10) 
		return sqr(pow( base, power/2, precision/2 ));
	if (power >= 1 ) 
		return base * pow( base, power-1, precision );
	if (precision >= 1 ) 
		return sqrt(base);
	return sqrt( pow( base, power*2, precision*2 ) );
}
double pow(double base, double power) { 
	return pow(base, power, .000001); 
}